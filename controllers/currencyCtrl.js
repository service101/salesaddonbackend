const Currency = require('../models/currency');

// Add a Currency to the database
module.exports.addCurrency = (reqBody) => {
	let newCurrency = new Currency ({
		currencyName: reqBody.currencyName,
		currencyTicker : reqBody.currencyTicker
	})


	return newCurrency.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the currency in the database!`;
		} else {
			return `Successfully added the currency in the database!`;
		}
	})

}

// View all Currencies from the database
module.exports.viewAllCurrencies = () => {
	return Currency.find().then(result => {
		return result;
	})
}

// View a specific Currency from the database
module.exports.viewCurrency = (reqParams) => {
	return Currency.find({currencyTicker: reqParams.currencyTicker}).then(result => {
		if(Object.keys(result).length < 1) {
			return `No currency with a ticker of ${reqParams.currencyTicker}`;
		} else {
			return result
		}
	})
}
