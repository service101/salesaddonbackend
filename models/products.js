const mongoose = require('mongoose')

const productsSchema = new mongoose.Schema({

    materialId: {
        type: String,
        required: true
    },
    materialDescription:{
        type: String,
        required: true
    },
    productCategoryId:{
        type: String,
        required: true
    },
    productCategoryDescription:{
        type: String,
        required: true
    },
    purchasingUom:{
        type: String,
        required: true
    },
    sellingUom:{
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    }

})

module.exports = mongoose.model('products', productsSchema);