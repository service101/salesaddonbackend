const express = require('express');
const shipToLocationCustomer = require('../controllers/shipToLocationCustomerCtrl');
const router = express.Router();

/* Add a UOM to the Database */
router.post('/add', (req, res) => {
	shipToLocationCustomer.addShipToLocationCustomer(req.body).then(result => res.send(result));
})

/* View all UOM from the Database */
router.get('/view/all', (req, res) => {
	shipToLocationCustomer.viewAllShipToLocationCustomers().then(result => res.send(result));
})

/* View a specific UOM from the Database */
router.get('/view/specific/:shipToPartyId', (req, res) => {
	shipToLocationCustomer.viewShipToLocationCustomer(req.params).then(result => res.send(result));
})


module.exports = router;