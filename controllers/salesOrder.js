const SalesOrder = require('../models/SalesOrderDb');
const axios = require('axios')

/*Add Sales Order*/
module.exports.addSalesOrder = (reqBody) => {

	let newSalesOrder = new SalesOrder({
        salesOrderNo: reqBody.salesOrderNo,
        accountId: reqBody.accountId,
        accountName: reqBody.accountName,
        shipToPartyId: reqBody.shipToPartyId,
        shipToPartyDescription: reqBody.shipToPartyDescription,
        paymentTermsId: reqBody.paymentTermsId,
        paymentTerms: reqBody.paymentTerms,
        requestedDate: reqBody.requestedDate,
        externalReference: reqBody.externalReference,
        comments: reqBody.comments,
        docStatus: reqBody.docStatus,
        creationDate: reqBody.creationDate,
        requestor: reqBody.requestor,
        employeeId: reqBody.employeeId,
        currency: reqBody.currency,
        sapSoId: '',
        items: reqBody.items
	})

	return newSalesOrder.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the sales order in the database!`;
		} else {
			return `Successfully added the sales order in the database!`;
		}
	})
};

/*Retrieve All Sales Order in the Database*/
module.exports.getAll = () => {
    return SalesOrder.find().then( result => {
        return result
    })
}

/*Retrieve All Sales Order in the Database Customer Specific*/
module.exports.getAllSalesOrder = (reqParams) => {
    return SalesOrder.find({employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve All Sales Order All Status (approverView)*/
module.exports.getAllStatusApprover = (reqParams) => {
    return SalesOrder.find({docStatus: 'Approved'}).then( result => {
        return result
    })
}


/*Retrieve All Sales Order With ForApproval Status (approverView)*/
module.exports.getAllForApproval = (reqParams) => {
    return SalesOrder.find({docStatus: 'For Approval'}).then( result => {
        return result
    })
}

/*Retrieve All Sales Order With Rejected Status (approverView)*/
module.exports.getAllRejected = (reqParams) => {
    return SalesOrder.find({docStatus: 'Rejected'}).then( result => {
        return result
    })
}
/*Retrieve All Sales Order With Approved Status (approverView)*/
module.exports.getAllApproved = (reqParams) => {
    return SalesOrder.find({docStatus: 'Approved'}).then( result => {
        return result
    })
}

/*Retrieve In Preparation Sales Order*/
module.exports.inPreparationSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'In Preparation', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve For Approval Sales Order*/
module.exports.forApprovalSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'For Approval', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Approved Sales Order*/
module.exports.approvedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Approved', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Posted Sales Order*/
module.exports.postedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Posted', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Rejected Sales Order*/
module.exports.rejectedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Rejected', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Specific Sales Order in the Database using Document ID*/
module.exports.getSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        return result
    })
} 

/*Edit the Retrieved Specific Sales Order to update in the Database*/
module.exports.editSalesOrder = (reqBody, reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then( result => {
        result.salesOrderNo = reqBody.salesOrderNo
        result.accountId = reqBody.accountId
        result.accountName = reqBody.accountName
        result.shipToPartyId = reqBody.shipToPartyId
        result.shipToPartyDescription = reqBody.shipToPartyDescription
        result.paymentTermsId = reqBody.paymentTermsId
        result.paymentTerms = reqBody.paymentTerms
        result.requestedDate = reqBody.requestedDate
        result.externalReference = reqBody.externalReference
        result.comments = reqBody.comments
        result.docStatus = reqBody.docStatus
        result.creationDate = reqBody.creationDate
        result.requestor = reqBody.requestor
        result.employeeId = reqBody.employeeId
        result.currency = reqBody.currency
        result.sapSoId = reqBody.sapSoId
        result.items = reqBody.items

        return result.save().then((user, error) => {
            if(error){
                return `Error: Failed to update the sales order in the database!`;
            } else {
                return `Successfully updated the sales order in the database!`;
            }
        })
    })
}

        
/*Edit the Status of the Retrieved Specific Sales Order to For Approval*/
module.exports.submitForApproval = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'For Approval'

        return result.save().then((user, error) => {
            if(error) {
                return `Error: Failed to update the sales order status in the database!`
            } else {
                return `Successfully updated the sales order status in the database!`
            }
        })
    }
)}

/*Edit the Status of the Retrieved Specific Sales Order to For Posting*/
 module.exports.submitForPosting = (reqParams) => {
     return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
         result.docStatus = 'Posted'

         return result.save().then((user, error) => {
             if(error) {
                 return `Error: Failed to update the sales order status in the database!`
             } else {
                 return `Successfully updated the sales order status in the database!`
             }
         })
     }
)}


// /*Edit the Status of the Retrieved Specific Sales Order to For Posting*/
// module.exports.submitForPosting = (reqParams) => {
//     return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {


// let header  = `<?xml version="1.0" encoding="utf-8"?>
// <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">
// <soapenv:Header/>
// <soapenv:Body>
// <n0:SalesOrderBundleMaintainRequest_sync
// xmlns:n0="http://sap.com/xi/SAPGlobal20/Global">
// <BasicMessageHeader/>`

// let body;

// body = result.map(body => {
//     return  (
//         body =
//         `
//         <SalesOrder>
// <BuyerID>SAMPLE EXTERNAL REF</BuyerID>
// <AccountParty>
// <PartyID>${result.accountId}</PartyID>
// </AccountParty>
// <BillToParty>
// <PartyID>${result.accountId}</PartyID>
// </BillToParty>
// <EmployeeResponsibleParty>
//                <PartyID>${result.employeeId}</PartyID>
//             </EmployeeResponsibleParty>
// <SalesAndServiceBusinessArea actionCode="01">
// <DistributionChannelCode>01</DistributionChannelCode>
// </SalesAndServiceBusinessArea>
// <RequestedFulfillmentPeriodPeriodTerms> 
//    <StartDateTime timeZoneCode="UTC">${result.requestedDate}</StartDateTime> 
//   </RequestedFulfillmentPeriodPeriodTerms> 
// <TextCollection> 
// <Text> <TypeCode>10011</TypeCode> <ContentText>This information is for the Customer</ContentText> 
// </Text> 
// </TextCollection>
// <Item>
// <ID>${result.items.productId}</ID>
// <ItemProduct>
// <ProductInternalID>${result.items.productDescirption}</ProductInternalID>
// </ItemProduct>
// <ItemScheduleLine>
// <Quantity>${result.items.quantity}</Quantity>
// </ItemScheduleLine>
// </Item>
// </SalesOrder>
//         `
//     )
// }).join("")



// let footer = `</n0:SalesOrderBundleMaintainRequest_sync>
// </soapenv:Body>
// </soapenv:Envelope>`

// let xml = header + body + footer

//     request({
//         method: 'POST',
//         uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/managesalesorderin5',
//         auth: {
//             'user': '_AOS0001',
//             'pass': '@dd0nP@$$',
//             'sendImmediately': false
//         },
//         headers: {
//             'Content-Type': 'text/xml'
//           },
//         body: xml,
//     },
//      function (error, response, body) {
//       console.error('error:', error); // Print the error if one occurred
//       console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
//       console.log('body:', body); // Print the HTML for the Google homepage.
// 		parseString(body, function(err, result){
//             let x = Object.values(result['soap-env:Envelope']['soap-env:Body']['0']['n0:AccountOpenAmountsQueryResponse_sync']['0']['AccountOpenAmounts']['0']['CreditLimitAmount']['0'])
//             res.send(x)
// 		})
//   })

//         result.docStatus = 'Posted'

//         return result.save().then((user, error) => {
//             if(error) {
//                 return `Error: Failed to update the sales order status in the database!`
//             } else {
//                 return `Successfully updated the sales order status in the database!`
//             }
//         })
//     }
// )}

// Delete Function
module.exports.deleteSalesOrder = (reqParams) => {
    return SalesOrder.deleteOne({salesOrderNo: reqParams.salesOrderNo}).then((result, error) => {
        if(error) {
            return `Error: Failed to delete the sales order in the database!`
        } else {
            return `Successfully delete the sales order in the database!`
        }
    }
)}

/*Approve Function*/
module.exports.approveSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'Approved'

        return result.save().then((result, error) => {
            if(error) {
                return `Error: Failed to Approve the Sales Order!`
            } else {
                return `Successfully Approved the Sales Order!`
            }
        })
    }
)}


/*Reject Function*/
module.exports.rejectSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'Rejected'

        return result.save().then((result, error) => {
            if(error) {
                return `Error: Failed Reject the Sales Order!`
            } else {
                return `Successfully Rejected the Sales Order!`
            }
        })
    }
)}

/*Post Function*/
module.exports.postSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'Posted'

        return result.save().then((result, error) => {
            if(error) {
                return `Error: Failed to Post the Sales Order!`
            } else {
                return `Successfully Posted the Sales Order!`
            }
        })
    }
)}
    
/*Retrieve All Status Sales Order  (approver)*/
module.exports.getAllStatusApprover = (reqParams) => {
    return SalesOrder.find({}).then( result => {
        let resultArray = []
        result.forEach(data => {
            if(data.docStatus === 'For Approval') {
                resultArray.push(data)
            } else if(data.docStatus === 'Approved') {
                resultArray.push(data)
            } else if(data.docStatus === 'Returned') {
                resultArray.push(data)
            } else if(data.docStatus === 'Rejected') {
                resultArray.push(data)
            }
        })

        return resultArray
    })
}





    
  

    
