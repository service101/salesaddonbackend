const request = require("request")
const parseString = require('xml2js').parseString;

module.exports.sendToByd = (params) => {
    request({
        method: 'POST',
        uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/managesalesorderin5',
        auth: {
            'user': '_AOS0001',
            'pass': '@dd0nP@$$',
            'sendImmediately': false
        },
        headers: {
            'Content-Type': 'text/xml'
          },
        body: params.body,
    },
     function (error, response, body) {
      console.error('error:', error); // Print the error if one occurred
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('body:', body); // Print the HTML for the Google homepage.
		parseString(body, function(err, result){
            let x = Object.values(result['soap-env:Envelope']['soap-env:Body']['0']['n0:AccountOpenAmountsQueryResponse_sync']['0']['AccountOpenAmounts']['0']['CreditLimitAmount']['0'])
            res.send(x)
		})
  })
}