const express = require('express');
const router = express.Router();
const salesOrderController = require('../controllers/salesOrder');

/*Save Sales Order*/
router.post('/add', (req, res) => {
    salesOrderController.addSalesOrder(req.body).then(result => res.send(result))
});

/*Retrieve All Sales Order in the Database*/
router.get('/allSalesOrderinDatabase', (req, res) => {
	salesOrderController.getAll().then(result => res.send(result))
})

/*Retrieve All Sales Order in the Database per Employee*/
router.get('/specificRequestor/:employeeId', (req, res) => {
	salesOrderController.getAllSalesOrder(req.params).then( result => res.send(result))
})

/*Retrieve All Status Sales Order  (approver)*/
// router.get('/status/allStatusApprover', (req, res) => {
// 	salesOrderController.getAllStatusApprover(req.params).then( result => res.send(result))
// })

/*Retrieve All Status Sales Order  (approver)*/
router.get('/status/allStatusApprover', (req, res) => {
    salesOrderController.getAllStatusApprover().then(result => res.send(result))
});

/*Retrieve All Sales Order With ForApproval Status (approver)*/
router.get('/status/allForApprovalStatus', (req, res) => {
	salesOrderController.getAllForApproval(req.params).then( result => res.send(result))
})

/*Retrieve All Sales Order With Rejected Status (approver)*/
router.get('/status/allRejected', (req, res) => {
	salesOrderController.getAllRejected(req.params).then( result => res.send(result))
})
/*Retrieve All Sales Order With Approved Status (approver)*/
router.get('/status/allApproved', (req, res) => {
	salesOrderController.getAllApproved(req.params).then( result => res.send(result))
})


/*Retrieve Specific Sales Order in the Database using Document ID*/
router.get('/salesOrderMonitoring/:salesOrderNo', (req, res) => {
	salesOrderController.getSalesOrder(req.params).then( result => res.send(result))
})

/*Edit the Retrieved Specific Sales Order to update in the Database*/
router.put('/salesOrderMonitoring/edit/:salesOrderNo', (req, res) => {
	salesOrderController.editSalesOrder(req.body, req.params).then( result => res.send(result))
})
 
/*Retrieve In Preparation Sales Order*/
router.get('/status/inPreparation/:employeeId', (req, res) => {
    salesOrderController.inPreparationSalesOrder(req.params).then(result => res.send(result))
});

/*Retrieve For Approval Sales Order*/
router.get('/status/forApproval/:employeeId', (req, res) => {
    salesOrderController.forApprovalSalesOrder(req.params).then(result => res.send(result))
});

/*Retrieve Approved Sales Order*/
router.get('/status/approved/:employeeId', (req, res) => {
    salesOrderController.approvedSalesOrder(req.params).then(result => res.send(result))
});

/*Retrieve Posted Sales Order*/
router.get('/status/posted/:employeeId', (req, res) => {
    salesOrderController.postedSalesOrder(req.params).then(result => res.send(result))
});

/*Retrieve Rejected Sales Order*/
router.get('/status/rejected/:employeeId', (req, res) => {
    salesOrderController.rejectedSalesOrder(req.params).then(result => res.send(result))
});

/*Edit the Status of the Retrieved Specific Sales Order to For Approval*/
router.put('/salesOrderMonitoring/submit/forApproval/:salesOrderNo', (req, res) => {
	salesOrderController.submitForApproval(req.params).then( result => res.send(result))
})

/*Edit the Status of the Retrieved Specific Sales Order to For Posting*/
router.put('/salesOrderMonitoring/submit/forPosting/:salesOrderNo', (req, res) => {
	salesOrderController.submitForPosting(req.params).then( result => res.send(result))
})

/*Delete the Retrieved Specific Sales Order*/
router.delete('/salesOrderMonitoring/delete/:salesOrderNo', (req, res) => {
	salesOrderController.deleteSalesOrder(req.params).then( result => res.send(result))
})

/*Post Function*/
router.put('/salesOrderMonitoring/post/:salesOrderNo', (req, res) => {
	salesOrderController.postSalesOrder(req.params).then( result => res.send(result))
})
/*Approve Function*/
router.put('/salesOrderMonitoring/approve/:salesOrderNo', (req, res) => {
	salesOrderController.approveSalesOrder(req.params).then( result => res.send(result))
})

/*Reject Function*/
router.put('/salesOrderMonitoring/reject/:salesOrderNo', (req, res) => {
	salesOrderController.rejectSalesOrder(req.params).then( result => res.send(result))
})

module.exports = router;
