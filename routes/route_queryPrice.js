const express = require('express');
const router = express.Router();
const queryPricetController = require('../controllers/queryPriceListWebService');

router.post('/priceList', (req, res) => {
    queryPricetController.queryPrice(req.params).then( result => res.send(result));
})

module.exports = router;