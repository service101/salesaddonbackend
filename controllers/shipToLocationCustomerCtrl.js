const ShipToLocationCustomer = require('../models/shipToLocationCustomer');

// Add a Ship To Location Customer to the Database
module.exports.addShipToLocationCustomer = (reqBody) => {
	let newShipToLocationCustomer = new ShipToLocationCustomer ({
		shipToPartyId: reqBody.shipToPartyId,
		shipToPartyDescription : reqBody.shipToPartyDescription
	})


	return newShipToLocationCustomer.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the currency in the database!`;
		} else {
			return `Successfully added the currency in the database!`;
		}
	})

}

// View All Ship To Location Customer from the Database
module.exports.viewAllShipToLocationCustomers = () => {
	return ShipToLocationCustomer.find().then(result => {
		return result;
	})
}

// View Specific Ship To Location Customer from the database
module.exports.viewShipToLocationCustomer = (reqParams) => {
	return ShipToLocationCustomer.find({shipToPartyId: reqParams.shipToPartyId}).then(result => {
		if(Object.keys(result).length < 1) {
			return `No Ship To Location Customer with an ID of ${reqParams.shipToPartyId}`;
		} else {
			return result
		}
	})
}