const Customer = require('../models/customer');

// Add a Customer
module.exports.addCustomer = (reqBody) => {
	let newCustomer = new Customer ({
		accountId: reqBody.accountId,
		accountName : reqBody.accountName,
		searchName : reqBody.searchName,
        address: reqBody.address,
		taxStatus: reqBody.taxStatus,
		currency: reqBody.currency,
		paymentTerms: reqBody.paymentTerms,
		employeeResponsible: reqBody.employeeResponsible
	})

	return newCustomer.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the company in the database!`;
		} else {
			return `Successfully added the company in the database!`;
		}
	})
}

// View All Customer 
module.exports.viewAllCustomers = () => {
    return Customer.find().then(result => {
        return result
    })
}

// Get specific  Customer
module.exports.viewCustomer = (reqParams) => {
	return Customer.findOne({accountName: reqParams.accountName}).then(result => {
		return result;
	})
}
