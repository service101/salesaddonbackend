const mongoose = require('mongoose')

const CustomerSchema = new mongoose.Schema({
    accountId: {
        type: String,
        require: true
    },
    accountName: {
        type: String,
        require: true
    },
    searchName: {
        type: String,
        require: true
    },
    address: {
        type: String,
        require: true
    },
    taxStatus: {
        type: String,
        require: true
    },
    currency: {
        type: String,
        require: true
    },
    paymentTerms: {
        type: String,
        require: true
    },
    employeeResponsible: {
        type: String,
        require: true
    }
})

module.exports = mongoose.model('Customer', CustomerSchema);
