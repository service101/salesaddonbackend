const mongoose = require('mongoose')

const shipToLocationCustomerSchema = new mongoose.Schema({

    shipToPartyId: {
        type: String,
        required: true
    },
    shipToPartyDescription:{
        type: String,
        required: true
    }

})

module.exports = mongoose.model('shipToLocationCustomer', shipToLocationCustomerSchema);